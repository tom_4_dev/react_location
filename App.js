import React, {Component} from 'react';
import { Alert, StyleSheet, Text, View } from 'react-native';
import axios from 'axios';

export default class App extends Component {
  state = {
    lat: "22.6131",
    lng: 88.4398,
    data: "detecting...",
    geoInfo: {
      address: "finding...",
      distance: "calculating..."
    }
  }

  componentDidMount(){
    this.getData();
    this.findCoordinates();
  }

  getData(){
    setTimeout(() => {
      var today = new Date();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      this.setState({
        data: time
      })
    }, 5000)
  }

  getGeoInfoFromServer = (lat, lng) => {
    reqUrl = "https://santattech.herokuapp.com/api/locations?lat="+ lat + "&lng=" + lng;
    axios.get(reqUrl)
      .then(res => {
        this.setState({geoInfo: res.data});
      }).catch(error => {
        console.log(error)
        //Alert.alert("something went wrong");
      })
  }

  findCoordinates = () => {
    setTimeout(() => {
      navigator.geolocation.getCurrentPosition(
        position => {
          const lat = JSON.stringify(position.coords.latitude);
          const lng = JSON.stringify(position.coords.longitude);
          this.getGeoInfoFromServer(lat, lng);
          this.setState({
            lat: lat,
            lng: lng
          })
        },
        error => Alert.alert(error.message),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );
    }, 10000);
  };

  render() {
    this.getData();
    this.findCoordinates();

    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
      }}>

        <View style={styles.header}>
          <Text style={styles.welcomeText}>Hello Santanu</Text>
        </View>
        <View style={styles.content}>
          <View style={{flex: 1}}>
            <Text style={styles.contentHeader}>Your current location</Text>
          </View>

        <View style={{flex: 5, justifyContent: 'flex-start', borderLeftWidth: 10, backgroundColor: '#222930'}}>
            <Text style={styles.contentHeader}>Time: {this.state.data}</Text>
            <Text style={styles.contentHeader}>{this.state.lat}, {this.state.lng}</Text>
        { Object.keys(this.state.geoInfo).map(k => {
          return <Text key={k}>
            <Text style={styles.distance}>{k.toUpperCase()}: </Text>
            <Text style={styles.content}>{this.state.geoInfo[k]}</Text>
            </Text>
        }) }
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    backgroundColor: '#4EB1BA',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcomeText: {
    color: 'white',
    fontSize: 23,
    fontWeight: 'bold',
  },
  content: {
    flex: 4.5,
    backgroundColor: '#222930',
    color: '#e9e9e9',
    alignItems: 'center'
  },
  contentHeader: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 8
  },
  distance: {
    color: '#E9E9E9',
    marginTop: 3,
    fontSize: 18,
    fontWeight: 'bold',
  }
});
